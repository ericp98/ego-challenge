**EGO CHALLENGE**

1. Ingresar al link de la API: `https://challenge.agenciaego.tech/models`. Acá se debe habilitar los permisos de SSL, para que la página pueda obtener los datos por medio de peticiones a la API en cuestión. 
[![](https://i.ibb.co/bF0cfPZ/Error.png)](https://i.ibb.co/bF0cfPZ/Error.png)

2. Luego de clonar el repositorio, ejecutar el siguiente comando: `npm install`

3. Ejecutar el comando `npm start`

4. El servidor estará disponible en la ruta: `http://localhost:3000/` 

5. La página se encuentra alojada en GitHub Pages. La misma puede ser accedida desde el link: `https://ericp98.github.io/ego-challenge/#/models`. En caso de acceder por este medio, **solamente se debe realizar el punto 1** e ignorar los restantes. 
