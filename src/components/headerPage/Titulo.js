import React from 'react'

function Titulo() {
    return (
        <div className="container-fluid title-padding">
            <div className="row justify-content-center">
                <div className="col-8"> 
                    <h2 className="header-title mt-5 mb-5">Descubrí todos los modelos </h2>
                </div> 
            </div>
        </div>
    )
}

export default Titulo;
