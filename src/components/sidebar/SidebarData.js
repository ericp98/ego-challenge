export const SidebarData = [
    {
        title: "Modelos",
        path: "/models",
        className: "nav-text"
    },
    {
        title: "Servicios y Accesorios",
        path: "#",
        className: "nav-text"
    },
    {
        title: "Financiacion",
        path: "#",
        className: "nav-text"
    },
    {
        title: "Reviews y Comunidad",
        path: "#",
        className: "nav-text"
    }]

export const SidebarData2 = [
    {
        title: "Toyota Mobility Service",
        path: "#",
        className: "nav-text"
    },
    {
        title: "Toyota Gazoo Racing",
        path: "#",
        className: "nav-text"
    },
    {
        title: "Toyota Híbridos",
        path: "#",
        className: "nav-text"
    },
]

export const SidebarData3 = [
    {
        title: "Concesionarios",
        path: "#",
        className: "nav-text"
    },
    {
        title: "Test Drive",
        path: "#",
        className: "nav-text"
    },
    {
        title: "Contacto",
        path: "#",
        className: "nav-text"
    },
]

export const SidebarData4 = [
    {
        title: "Actividades",
        path: "#",
        className: "nav-text"
    },
    {
        title: "Servicios al Cliente",
        path: "#",
        className: "nav-text"
    },
    {
        title: "Ventas Especiales",
        path: "#",
        className: "nav-text"
    },
    {
        title: "Innovación",
        path: "#",
        className: "nav-text"
    },
    {
        title: "Prensa",
        path: "#",
        className: "nav-text"
    },
    {
        title: "Acerca de...",
        path: "#",
        className: "nav-text"
    }
]